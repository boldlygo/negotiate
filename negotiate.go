// Package negotiate provides RFC 7231 content negotiation.
package negotiate

import (
	"errors"
	"mime"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// Common HTTP headers.
const (
	HeaderAccept         = "Accept"
	HeaderAcceptCharset  = "Accept-Charset"
	HeaderAcceptEncoding = "Accept-Encoding"
	HeaderAcceptLanguage = "Accept-Language"

	HeaderContentType     = "Content-Type"
	HeaderContentEncoding = "Content-Encoding"
	HeaderContentLanguage = "Content-Language"
)

// TODO: improve errors.

var (
	// errMalformed     = errors.New("malformed")
	errNotAcceptable = errors.New("not acceptable")
	errNoMatch       = errors.New("no match")
	errNoPreference  = errors.New("no preference")
	errOutOfRange    = errors.New("out of range")
)

type value struct {
	Value  string
	Params map[string]string
	Q      float64
	Wilds  int
}

// 5.3.1: The weight is normalized to a real number in the range 0 through 1 [...]
// A sender of qvalue MUST NOT generate more than three digits after the decimal point.
func parseWeight(s string) (float64, error) {
	q, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, err
	}

	if q == 0 {
		return q, nil
	}

	if q > 1 || q < 0.001 {
		return 0, errOutOfRange
	}

	return q, nil
}

func parse(s string) (value, error) {
	var v value

	mt, params, err := mime.ParseMediaType(s)
	if err != nil {
		return v, err
	}

	v = value{
		Value:  mt,
		Params: params,
		Q:      1,
		Wilds:  strings.Count(mt, "*"),
	}

	if v.Params["q"] != "" {
		q, err := parseWeight(v.Params["q"])
		if err != nil {
			return v, err
		}

		v.Q = q
	}

	delete(v.Params, "q")

	return v, nil
}

func (v value) String() string {
	s := v.Value

	// ensure consistent order of params
	keys := make(sort.StringSlice, 0, len(v.Params))
	for key := range v.Params {
		keys = append(keys, key)
	}

	keys.Sort()

	for _, key := range keys {
		s += "; " + key + "=" + v.Params[key]
	}

	return s
}

type values []value

func parseList(s string) (values, error) {
	var (
		x    = strings.Split(s, ",")
		list = make(values, 0, len(x))
	)

	for _, v := range x {
		val, err := parse(v)
		if err != nil {
			return nil, err
		}

		list = append(list, val)
	}

	sort.Stable(list)

	return list, nil
}

func (p values) Len() int { return len(p) }

func (p values) Less(i, j int) bool {
	if p[i].Q != p[j].Q {
		return p[i].Q > p[j].Q
	}

	if len(p[i].Params) != 0 && len(p[j].Params) == 0 {
		return true
	}

	if len(p[i].Params) == 0 && len(p[j].Params) != 0 {
		return false
	}

	return p[i].Wilds < p[j].Wilds
}

func (p values) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

// Matcher keeps a set of supported options.
type Matcher struct {
	offers values
	cache  map[string]value // cache of prior calls to negotiate()
	mu     sync.RWMutex     // protects cache
}

// New returns a new Matcher with the provided offers.
func New(offers ...string) (*Matcher, error) {
	m := Matcher{
		cache: make(map[string]value, len(offers)),
	}

	for _, offer := range offers {
		v, err := parse(offer)
		if err != nil {
			return nil, err
		}

		if v.Wilds > 0 {
			return nil, errors.New("wildcards are not allowed in offers")
		}

		m.offers = append(m.offers, v)
	}

	sort.Stable(m.offers)

	return &m, nil
}

// pick builds a list of values from matches, initially ordered by offered and then sorts them and returns the first
// one in the list.
func pick(offers values, matches map[string]value) value {
	list := make(values, 0, len(offers)+1)
	for i := range offers {
		list = append(list, matches[offers[i].String()])
	}

	list = append(list, value{}) // the "fallback"

	sort.Stable(list)

	return list[0]
}

func negotiate(offers, accepts values) value {
	matches := make(map[string]value, offers.Len())

	for i := range accepts {
		var (
			accept = accepts[i].String()
			prefix = strings.Split(accept, "*")[0] // get up to first wildcard
		)

		for j := range offers {
			var (
				offer = offers[j].String()
				parts = strings.Split(offers[j].Value, "/")
			)

			// does offer match accept?
			switch {
			// yes
			case offer == accept: // exact w/parameters
			case offers[j].Value == accept: // exact w/o parameters
			case strings.HasPrefix(offer, prefix): // wildcard w/prefix
			case len(parts) == accepts[i].Wilds: // wildcard w/o prefix

			// no
			default:
				continue
			}

			var (
				match, ok = matches[offer]
				replace   = value{
					Value:  offers[j].Value,
					Params: offers[j].Params,
					Q:      accepts[i].Q * offers[j].Q,
					Wilds:  accepts[i].Wilds,
				}
			)

			// replace the match?
			switch {
			// yes
			case !ok:
			case accept == offer:
			case replace.Q > match.Q:
			case replace.Wilds < match.Wilds:

			// no
			default:
				continue
			}

			matches[offer] = replace
		}
	}

	return pick(offers, matches)
}

// Match selects the most appropriate media type provided by m that matches accept.
func (m *Matcher) Match(accept string) (string, error) {
	if len(m.offers) == 0 {
		return "", errNoPreference
	}

	// fast path
	m.mu.RLock()
	match, ok := m.cache[accept]
	m.mu.RUnlock()

	if !ok {
		// slow path
		accepts, err := parseList(accept)
		if err != nil {
			return "", err
		}

		match = negotiate(m.offers, accepts)

		m.mu.Lock()
		m.cache[accept] = match
		m.mu.Unlock()
	}

	switch {
	case match.Value == "":
		return "", errNoMatch
	case match.Q == 0:
		return match.String(), errNotAcceptable
	default:
		return match.String(), nil
	}
}

// ResponseContentType inspects the Accept header(s) of r and selects the most appropriate response media type offered
// by m. If there are no errors, the Content-Type header of w is set to the returned media type. Otherwise, the error
// is returned.
func ResponseContentType(w http.ResponseWriter, r *http.Request, m *Matcher, errorHandler interface{}) (string, error) {
	// To support go1.13, go1.12
	ct, err := m.Match(strings.Join(r.Header[http.CanonicalHeaderKey(HeaderAccept)], ", "))
	// TODO: after go1.13 support is dropped, change to:
	// ct, err := m.Match(strings.Join(r.Header.Values(HeaderAccept), ", "))

	if err == nil {
		w.Header().Set(HeaderContentType, ct)
	}

	return ct, err
}
