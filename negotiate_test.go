package negotiate

import (
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func Test_parseWeight(t *testing.T) {
	type args struct {
		s string
	}

	tests := []struct {
		name    string
		args    args
		want    float64
		wantErr bool
	}{
		{"", args{"1"}, 1, false},
		{"", args{"1.0"}, 1, false},
		{"", args{"0.1"}, 0.1, false},
		{"", args{"0.01"}, 0.01, false},
		{"", args{"0.001"}, 0.001, false},
		{"", args{"0"}, 0, false},
		{"", args{"0.0"}, 0, false},
		{"", args{""}, 0, true},
		{"", args{"1.001"}, 0, true},
		{"", args{"0.0001"}, 0, true},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseWeight(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseWeight() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("parseWeight() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseValue(t *testing.T) {
	type args struct {
		s string
	}

	tests := []struct {
		name    string
		args    args
		want    value
		wantErr bool
	}{
		{"", args{"*/*"}, value{"*/*", nil, 1, 2}, false},
		{"", args{"audio/*; q=0.2"}, value{"audio/*", nil, 0.2, 1}, false},
		{"", args{"audio/basic"}, value{"audio/basic", nil, 1, 0}, false},
		{"", args{"text/plain;format=flowed"}, value{"text/plain", map[string]string{"format": "flowed"}, 1, 0}, false},

		{"", args{"unicode-1-1;q=0.8"}, value{"unicode-1-1", nil, 0.8, 0}, false},

		{"", args{"compress;q=0.5"}, value{"compress", nil, 0.5, 0}, false},

		{"", args{"en;q=0.7"}, value{"en", nil, 0.7, 0}, false},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := parse(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := cmp.Diff(tt.want, got, cmpopts.EquateEmpty()); diff != "" {
				t.Errorf("parseValue() output mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func Test_parseAcceptList(t *testing.T) {
	type args struct {
		s string
	}

	tests := []struct {
		name    string
		args    args
		want    values
		wantErr bool
	}{
		{
			"",
			args{"audio/*; q=0.2, audio/basic"},
			values{
				value{"audio/basic", nil, 1, 0},
				value{"audio/*", nil, 0.2, 1},
			},
			false,
		},
		{
			"",
			args{"text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c"},
			values{
				value{"text/html", nil, 1, 0},
				value{"text/x-c", nil, 1, 0},
				value{"text/x-dvi", nil, 0.8, 0},
				value{"text/plain", nil, 0.5, 0},
			},
			false,
		},
		{
			"",
			args{"text/*, text/plain, text/plain;format=flowed, */*"},
			values{
				value{"text/plain", map[string]string{"format": "flowed"}, 1, 0},
				value{"text/plain", nil, 1, 0},
				value{"text/*", nil, 1, 1},
				value{"*/*", nil, 1, 2},
			},
			false,
		},
		{
			"",
			args{"text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5"},
			values{
				value{"text/html", map[string]string{"level": "1"}, 1, 0},
				value{"text/html", nil, 0.7, 0},
				value{"*/*", nil, 0.5, 2},
				value{"text/html", map[string]string{"level": "2"}, 0.4, 0},
				value{"text/*", nil, 0.3, 1},
			},
			false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseList(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseAcceptList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := cmp.Diff(tt.want, got, cmpopts.EquateEmpty()); diff != "" {
				t.Errorf("parseAcceptList() output mismatch (-want +got):\n%s", diff)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		offers []string
	}

	tests := []struct {
		name    string
		args    args
		want    *Matcher
		wantErr bool
	}{
		{
			"",
			args{[]string{"application/json", "text/plain"}},
			&Matcher{
				offers: values{
					value{"application/json", map[string]string{}, 1, 0},
					value{"text/plain", map[string]string{}, 1, 0},
				},
				cache: map[string]value{},
			},
			false,
		},
		{
			"",
			args{[]string{"application/json; q=1", "text/plain; q=0.5"}},
			&Matcher{
				offers: values{
					value{"application/json", map[string]string{}, 1, 0},
					value{"text/plain", map[string]string{}, 0.5, 0},
				},
				cache: map[string]value{},
			},
			false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.offers...)
			if (err != nil) != tt.wantErr {
				t.Errorf("matcher.match() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatcherMatch(t *testing.T) {
	type fields struct {
		offers  values
		matches map[string]value
	}

	type args struct {
		want string
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			"no offers",
			fields{},
			args{"text/plain; q=0.5, text/html"},
			"",
			true,
		},
		{
			"prior match",
			fields{
				offers:  values{{}},
				matches: map[string]value{"text/plain; q=0.5, text/html": {Value: "text/html", Q: 1}},
			},
			args{"text/plain; q=0.5, text/html"},
			"text/html",
			false,
		},
		{
			"",
			fields{
				offers: values{
					value{"text/html", map[string]string{"level": "1"}, 1, 0},
					value{"text/html", map[string]string{"level": "2"}, 1, 0},
				},
				matches: map[string]value{},
			},
			args{"text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5"},
			"text/html; level=1",
			false,
		},
		{
			"",
			fields{
				offers: values{
					value{"text/html", map[string]string{"level": "2"}, 1, 0},
					value{"text/html", map[string]string{"level": "3"}, 1, 0},
				},
				matches: map[string]value{},
			},
			args{"text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5"},
			"text/html; level=3",
			false,
		},
		{
			"",
			fields{
				offers: values{
					value{"text/plain", nil, 1, 0},
				},
				matches: map[string]value{},
			},
			args{"text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5"},
			"text/plain",
			false,
		},
		{
			"",
			fields{
				offers: values{
					value{"application/json; charset=utf-8", nil, 1, 0},
					value{"text/plain; charset=utf-8", nil, 1, 0},
				},
				matches: map[string]value{},
			},
			args{"text/*;q=0.3, text/html;q=0.7, text/html;level=1, text/html;level=2;q=0.4, */*;q=0.5"},
			"application/json; charset=utf-8",
			false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			m := Matcher{
				offers: tt.fields.offers,
				cache:  tt.fields.matches,
			}
			got, err := m.Match(tt.args.want)
			if (err != nil) != tt.wantErr {
				t.Errorf("matcher.match() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("matcher.match() = %v, want %v", got, tt.want)
			}
		})
	}
}
